# tp6 linux

## partie 1


### dans backup 

```bash=
[baptiste@backup ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    5G  0 disk
sr0          11:0    1 1024M  0 rom
```
le nouveau disque est sdb
on ajoute un nouveau volume physique
```bash=
[baptiste@backup ~]$ sudo pvcreate /dev/sdb
[sudo] password for baptiste:
  Physical volume "/dev/sdb" successfully created.
```
On peut voir le résultat avec **sudo pvs** et **sudo pvdisplay**

sudo pvs :
```bash=
[baptiste@backup ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   5.00g 5.00g
```
sudo pvdisplay :
```bash=
[baptiste@backup ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               rl
  PV Size               <7.00 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              1791
  Free PE               0
  Allocated PE          1791
  PV UUID               cgkGlH-MhQ8-QS93-NRs9-Iqq7-xfeF-jH6Kbu

  "/dev/sdb" is a new physical volume of "5.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               5.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               I6dpBi-L4YE-G45d-yjgp-RSph-Tldn-4fX4Nv
```
on ajoute un nouveau volume group 

```bash=
[baptiste@backup ~]$ sudo vgcreate backup /dev/sdb
  Volume group "backup" successfully created
```

On peut voir le résultat avec **sudo pvs** et **sudo pvdisplay**

sudo pvs :
```bash=
[baptiste@backup ~]$ sudo pvs
  PV         VG     Fmt  Attr PSize  PFree
  /dev/sda2  rl     lvm2 a--  <7.00g     0
  /dev/sdb   backup lvm2 a--  <5.00g <5.00g
```

sudo pvdisplay :
```bash=
[baptiste@backup ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sdb
  VG Name               backup
  PV Size               5.00 GiB / not usable 4.00 MiB
  Allocatable           yes
  PE Size               4.00 MiB
  Total PE              1279
  Free PE               1279
  Allocated PE          0
  PV UUID               I6dpBi-L4YE-G45d-yjgp-RSph-Tldn-4fX4Nv

  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               rl
  PV Size               <7.00 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              1791
  Free PE               0
  Allocated PE          1791
  PV UUID               cgkGlH-MhQ8-QS93-NRs9-Iqq7-xfeF-jH6Kbu
```
on ajoute un nouveau volume logique
```bash=
[baptiste@backup ~]$ sudo lvcreate -l 100%FREE backup -n last_backup
  Logical volume "last_backup" created.
```
On peut voir le résultat avec **sudo lvs** et **sudo lvdisplay**

sudo lvs :
```bash=
[baptiste@backup ~]$ sudo lvs
  LV          VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  last_backup backup -wi-a-----  <5.00g
  root        rl     -wi-ao----  <6.20g
  swap        rl     -wi-ao---- 820.00m
```
sudo lvdisplay :
```bash=
[baptiste@backup ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/backup/last_backup
  LV Name                last_backup
  VG Name                backup
  LV UUID                fUGwQo-3L4Q-O4Rd-CMEl-ws62-lIM1-dLeduC
  LV Write Access        read/write
  LV Creation host, time backup.tp6.linux, 2021-12-09 16:04:30 +0100
  LV Status              available
  # open                 0
  LV Size                <5.00 GiB
  Current LE             1279
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2

  --- Logical volume ---
  LV Path                /dev/rl/swap
  LV Name                swap
  VG Name                rl
  LV UUID                2qqM33-0vkR-oP3u-puvX-iUT0-NXc0-jUcI7G
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2021-11-23 15:51:12 +0100
  LV Status              available
  # open                 2
  LV Size                820.00 MiB
  Current LE             205
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:1

  --- Logical volume ---
  LV Path                /dev/rl/root
  LV Name                root
  VG Name                rl
  LV UUID                h2IbnV-Nj8f-pfw2-25t9-D5oP-eicG-94nGkZ
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2021-11-23 15:51:13 +0100
  LV Status              available
  # open                 1
  LV Size                <6.20 GiB
  Current LE             1586
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:0
```
On formatere la partition en ext4
```bash=
[baptiste@backup ~]$ sudo mkfs -t ext4 /dev/backup/last_backup
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 1309696 4k blocks and 327680 inodes
Filesystem UUID: 79483d11-efa8-48c4-a868-35e48a8c58ee
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```
```bash=
[baptiste@backup ~]$ sudo mkdir /mnt/backup1
```
on monte de la partition
```bash=
[baptiste@backup ~]$ sudo mount /dev/backup/last_backup /mnt/backup1
```

```bash=
[baptiste@backup ~]$ mount
sysfs on /sys type sysfs (rw,nosuid,nodev,noexec,relatime,seclabel)
proc on /proc type proc (rw,nosuid,nodev,noexec,relatime)
devtmpfs on /dev type devtmpfs (rw,nosuid,seclabel,size=482036k,nr_inodes=120509,mode=755)
securityfs on /sys/kernel/security type securityfs (rw,nosuid,nodev,noexec,relatime)
tmpfs on /dev/shm type tmpfs (rw,nosuid,nodev,seclabel)
devpts on /dev/pts type devpts (rw,nosuid,noexec,relatime,seclabel,gid=5,mode=620,ptmxmode=000)
tmpfs on /run type tmpfs (rw,nosuid,nodev,seclabel,mode=755)
tmpfs on /sys/fs/cgroup type tmpfs (ro,nosuid,nodev,noexec,seclabel,mode=755)
cgroup on /sys/fs/cgroup/systemd type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,xattr,release_agent=/usr/lib/systemd/systemd-cgroups-agent,name=systemd)
pstore on /sys/fs/pstore type pstore (rw,nosuid,nodev,noexec,relatime,seclabel)
bpf on /sys/fs/bpf type bpf (rw,nosuid,nodev,noexec,relatime,mode=700)
cgroup on /sys/fs/cgroup/perf_event type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,perf_event)
cgroup on /sys/fs/cgroup/net_cls,net_prio type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,net_cls,net_prio)
cgroup on /sys/fs/cgroup/freezer type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,freezer)
cgroup on /sys/fs/cgroup/blkio type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,blkio)
cgroup on /sys/fs/cgroup/cpu,cpuacct type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,cpu,cpuacct)
cgroup on /sys/fs/cgroup/pids type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,pids)
cgroup on /sys/fs/cgroup/rdma type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,rdma)
cgroup on /sys/fs/cgroup/memory type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,memory)
cgroup on /sys/fs/cgroup/devices type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,devices)
cgroup on /sys/fs/cgroup/hugetlb type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,hugetlb)
cgroup on /sys/fs/cgroup/cpuset type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,cpuset)
none on /sys/kernel/tracing type tracefs (rw,relatime,seclabel)
configfs on /sys/kernel/config type configfs (rw,relatime)
/dev/mapper/rl-root on / type xfs (rw,relatime,seclabel,attr2,inode64,logbufs=8,logbsize=32k,noquota)
selinuxfs on /sys/fs/selinux type selinuxfs (rw,relatime)
systemd-1 on /proc/sys/fs/binfmt_misc type autofs (rw,relatime,fd=36,pgrp=1,timeout=0,minproto=5,maxproto=5,direct,pipe_ino=18181)
debugfs on /sys/kernel/debug type debugfs (rw,relatime,seclabel)
hugetlbfs on /dev/hugepages type hugetlbfs (rw,relatime,seclabel,pagesize=2M)
mqueue on /dev/mqueue type mqueue (rw,relatime,seclabel)
/dev/sda1 on /boot type xfs (rw,relatime,seclabel,attr2,inode64,logbufs=8,logbsize=32k,noquota)
tmpfs on /run/user/1000 type tmpfs (rw,nosuid,nodev,relatime,seclabel,size=99960k,mode=700,uid=1000,gid=1000)
/dev/mapper/backup-last_backup on /mnt/backup1 type ext4 (rw,relatime,seclabel)
```
On montre que la partition est bien montée
```bash=
[baptiste@backup ~]$ df -h
Filesystem                      Size  Used Avail Use% Mounted on
devtmpfs                        471M     0  471M   0% /dev
tmpfs                           489M     0  489M   0% /dev/shm
tmpfs                           489M  6.6M  482M   2% /run
tmpfs                           489M     0  489M   0% /sys/fs/cgroup
/dev/mapper/rl-root             6.2G  2.1G  4.2G  34% /
/dev/sda1                      1014M  266M  749M  27% /boot
tmpfs                            98M     0   98M   0% /run/user/1000
/dev/mapper/backup-last_backup  4.9G   20M  4.6G   1% /mnt/backup1

sudo nano /etc/fstab
...
/dev/backup/last_backup /mnt/backup1 ext4 defaults 0 0
```

```bash=
[baptiste@backup ~]$ sudo umount /mnt/backup1
```

```bash=
[baptiste@backup ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/backup1 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/backup1             : successfully mounted
```

```bash=
[baptiste@backup ~]$ sudo reboot
Connection to 10.5.1.13 closed by remote host.
Connection to 10.5.1.13 closed.
```

## partie 2

### toujours dans backup

***on setup le service NFS***
```bash=
[baptiste@backup ~]$ sudo mkdir /backup/
[sudo] password for baptiste:
```
on crée le service web.tp6.linux dans le dossier backup
```bash=
[baptiste@backup ~]$ sudo mkdir /backup/web.tp6.linux/
```
on crée le service db.tp6.linux dans le dossier backup
```bash=
[baptiste@backup ~]$ sudo mkdir /backup/db.tp6.linux/
```
On install le service NFS
```bash=
[baptiste@backup ~]$ sudo dnf install nfs-utils
...
Installed:
  gssproxy-0.8.0-19.el8.x86_64               keyutils-1.5.10-9.el8.x86_64          libevent-2.1.8-5.el8.x86_64
  libverto-libevent-0.3.0-5.el8.x86_64       nfs-utils-1:2.3.3-46.el8.x86_64       rpcbind-1.2.5-8.el8.x86_64

Complete!
```
configuration du serveur NFS

configuration du fichier /etc/idmapd.conf
```bash=
[baptiste@backup ~]$ sudo nano /etc/idmapd.conf
...
# The default is the host's DNS domain name.
Domain = tp6.linux

# In multi-domain environments, some NFS servers will append the identity
...
```
configuration du fichier /etc/idmapd.conf
```bash=
[baptiste@backup ~]$ sudo nano /etc/exports
/backup/web.tp6.linux/ 10.5.1.0/24(rw,no_root_squash)
/backup/db.tp6.linux/ 10.5.1.0/24(rw,no_root_squash)
```

(rw,no_root_squash) = l'utilisateur root peut lire et écrire si et seulement si root est en local.

On lance le service nfs-server :
```bash=
[baptiste@backup ~]$ systemctl enable nfs-server
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: baptiste
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: baptiste
Password:
==== AUTHENTICATION COMPLETE ====
```

```bash=
[baptiste@backup ~]$  systemctl status nfs-server
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabled; vendor preset: disabled)
  Drop-In: /run/systemd/generator/nfs-server.service.d
           └─order-with-mounts.conf
   Active: inactive (dead)
```

```bash=
[baptiste@backup ~]$  systemctl restart nfs-server
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to restart 'nfs-server.service'.
Authenticating as: baptiste
Password:
==== AUTHENTICATION COMPLETE ====
[baptiste@backup ~]$  systemctl status nfs-server
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabled; vendor preset: disabled)
  Drop-In: /run/systemd/generator/nfs-server.service.d
           └─order-with-mounts.conf
   Active: active (exited) since Thu 2021-12-09 16:53:44 CET; 3s ago
  Process: 28383 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl reload gssproxy ; fi (code=>
  Process: 28371 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
  Process: 28370 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=0/SUCCESS)
 Main PID: 28383 (code=exited, status=0/SUCCESS)
```
On configure les firewall
```bash=
[baptiste@backup ~]$ sudo firewall-cmd --add-port=2049/tcp --permanent
[sudo] password for baptiste:
success
```

```bash=
[baptiste@backup ~]$ sudo firewall-cmd --reload
success
```
On prouve que la machine écoute sur le port 2049 :
```bash=
[baptiste@backup ~]$ sudo ss -ltnp
State  Recv-Q Send-Q Local Address:Port  Peer Address:PortProcess
...
LISTEN 0      64           0.0.0.0:2049       0.0.0.0:*
...
LISTEN 0      64              [::]:2049          [::]:*
```

## partie 3
### dans web et db

**WEB**

on installe nfs-utils sur la vm web
```bash=
[baptiste@web ~]$ sudo dnf install nfs-utils
[sudo] password for baptiste:
...
Installed:
  gssproxy-0.8.0-19.el8.x86_64               keyutils-1.5.10-9.el8.x86_64          libevent-2.1.8-5.el8.x86_64
  libverto-libevent-0.3.0-5.el8.x86_64       nfs-utils-1:2.3.3-46.el8.x86_64       rpcbind-1.2.5-8.el8.x86_64

Complete!
```
on crée le dossier /srv/backup
```bash=
[baptiste@web ~]$ sudo mkdir /srv/backup
```
et on ajoute ***Domain = tp6.linux*** à ***etc/idmapd.conf***
```bash=
[baptiste@web backup]$ sudo nano /etc/idmapd.conf
...
# The default is the host's DNS domain name.
Domain = tp6.linux

# In multi-domain environments, some NFS servers will append the identity
...
```
puis on monte /backup/web.tp6.linux/ à /srv/backup
```bash=
[baptiste@web backup]$ sudo mount -t nfs 10.5.1.13:/backup/web.tp6.linux/ /srv/backup
```


```bash=
[baptiste@web backup]$ df -h
Filesystem                       Size  Used Avail Use% Mounted on
devtmpfs                         471M     0  471M   0% /dev
tmpfs                            489M     0  489M   0% /dev/shm
tmpfs                            489M  6.7M  482M   2% /run
tmpfs                            489M     0  489M   0% /sys/fs/cgroup
/dev/mapper/rl-root              6.2G  2.8G  3.5G  45% /
/dev/sda1                       1014M  266M  749M  27% /boot
tmpfs                             98M     0   98M   0% /run/user/1000
10.5.1.13:/backup/web.tp6.linux  6.2G  2.1G  4.1G  34% /srv/backup
```

```bash=
[baptiste@web backup]$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Tue Nov 23 14:51:17 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=f10fdcdb-194f-4ed0-b12b-ef23a862c5aa /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
```

```bash=
[baptiste@web ~]$ sudo mkdir srv
[baptiste@web ~]$ cd srv/
[baptiste@web srv]$ sudo mkdir backup

/backup/web.tp6.linux/ /srv/backup ext4 defaults 0 0
```


**DB**

```bash=
[baptiste@db ~]$ sudo dnf install nfs-utils
...
Installed:
  gssproxy-0.8.0-19.el8.x86_64               keyutils-1.5.10-9.el8.x86_64          libevent-2.1.8-5.el8.x86_64
  libverto-libevent-0.3.0-5.el8.x86_64       nfs-utils-1:2.3.3-46.el8.x86_64       rpcbind-1.2.5-8.el8.x86_64

Complete!
```

```bash=
[baptiste@db ~]$ sudo mkdir /srv/backup
```

```bash=
[baptiste@web backup]$ sudo nano /etc/idmapd.conf
...
# The default is the host's DNS domain name.
Domain = tp6.linux
```

```bash=
[baptiste@db ~]$ sudo mount -t nfs 10.5.1.13:/backup/db.tp6.linux/ /srv/backup
```

```bash=
[baptiste@db ~]$ sudo mount /backup/db.tp6.linux/ /srv/backup
[sudo] password for baptiste:
mount: /srv/backup: special device /backup/db.tp6.linux/ does not exist.
```

```bash=
[baptiste@db ~]$ sudo mount -t nfs 10.5.1.13:/backup/db.tp6.linux/ /srv/backup
[sudo] password for baptiste:
[baptiste@db ~]$ df -h
Filesystem                       Size  Used Avail Use% Mounted on
devtmpfs                         471M     0  471M   0% /dev
tmpfs                            489M     0  489M   0% /dev/shm
tmpfs                            489M  6.7M  482M   2% /run
tmpfs                            489M     0  489M   0% /sys/fs/cgroup
/dev/mapper/rl-root              6.2G  2.5G  3.8G  40% /
/dev/sda1                       1014M  266M  749M  27% /boot
tmpfs                             98M     0   98M   0% /run/user/1000
10.5.1.13:/backup/web.tp6.linux  6.2G  2.1G  4.2G  34% /srv/backup
```


# partie 4


### Scripte Sauvegarde Web dans backup_web

```bash=
tar czvf nextcloud_$date_web_tar.gz var/www/nextcloud
mv czvf nextcloud_$date_web_tar.gz /srv/backup/nextcloud_$date_web_tar.gz
sudo mkdir /var/log/backup/nextcloud_$date_web_tar.gz
exec &> [ $date ] Backup /srv/backup/nextcloud_$date_web_tar.gz created successfully.
txt=$"Backup /srv/backup/nextcloud_$date_web_tar.gz created successfully."
printf $txt
```

fin script

```bash=
sudo systemctl enable backup_web.service
sudo systemctl start backup_web.service

sudo nano /etc/systemd/system/backup.timer
[Unit]
Description=Lance backup.service à intervalles réguliers
Requires=backup.service

[Timer]
Unit=backup.service
OnCalendar=hourly

[Install]
WantedBy=timers.target
```




### Scripte db: dans backup_db

```bash=
date=$" date "
mysqldump -h 10.5.1.1 -p -u super_user super_base > super_dump.sql
tar czvf nextcloud_$date_db_tar.gz super_dump.sql
mv czvf nextcloud_$date_db_tar.gz /srv/backup/nextcloud_$date_db_tar.gz
sudo mkdir /var/log/backup/backup_db.log
exec &> [ $date ] Backup /srv/backup/nextcloud_$date_db_tar.gz created successfully.
txt=$"Backup /srv/backup/nextcloud_$date_db_tar.gz created successfully."
printf $txt
```
fin script

```bash=
sudo systemctl enable backup_db.service
sudo systemctl start backup_db.service
```
