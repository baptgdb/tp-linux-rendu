# tp 5 linux


## partie db.tp5.linux

on commence par installer mariadb.

```bash=
sudo dnf install mariadb-server
Last metadata expiration check: 1:05:38 ago on Thu 25 Nov 2021 04:08:27 PM CET.
Dependencies resolved.
=====================================================================================================================
 Package                          Architecture Version                                         Repository       Size
=====================================================================================================================
Installing:
...

Complete!
```

on regarde l'états du process:
```bash=
systemctl status mariadb | grep Active
   Active: active (running) since Thu 2021-11-25 17:20:50 CET; 2min 9s ago
```

on regarde les ports d'ecoute du process:
mysqld = MariaDB

```bash=
sudo ss -tunlp | grep mysqld | awk '{print $5}'
[sudo] password for baptiste:
*:3306
```

on listent les processus de MariaDB, avec "ps -ef | grep mysqld"
```bash=
ps -ef | grep mysqld
mysql       5295       1  0 17:20 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr
baptiste    5413    2143  0 17:32 pts/0    00:00:00 grep --color=auto mysqld
```

l'utilisateur de mysql est "mysql"


accès de mariadb au port TCP 3306

```bash=
sudo firewall-cmd --add-port=3306/tcp --permanent
[sudo] password for baptiste:
success
```
RELOAD !
```bash=
 sudo firewall-cmd --reload
success
```

puis avec la commande "sudo mysql_secure_installation" on fait les configuration élémentaire de la base.

et on répond aux questions.

```bash=
Configuration élémentaire de la base

[baptiste@db ~]$ sudo mysql_secure_installation
[sudo] password for baptiste:

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!
      
Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] Y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!
```

est-ce qu'on veut donner un password a l'utilisateur root ?
oui pour la sécuritée, c'est mieux que rien.

```bash=
...
By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smootoer.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] Y
 ... Success!
```
est-ce qu'on veut supprimer les utilisateurs anonymes,
oui, on ne les utiliseras pas et ce pourait posser des problèmes de sécuritée.
```bash=
...
Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] Y
 ... Success!
```
 est-ce qu'on veut interdire la connexion root à distance ?
 oui car on est en local et si on a besoin de root on peut toujours s'y connecter, on ne l'utiliseras pas a distance.
 
```bash=
...
By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] Y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!
```
est-ce qu'on veut supprimer la base de donnée publique de test ouvert à tous les individus de la planète ?
oui, ne pas le faire me laisse penser que ce serait parfait pour des exploites, si il y en as à utiliser à cette endroit là, dans tous les cas c'est pas super. 

```bash=
Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] Y
 ... Success!
```
est-ce qu'on veut sauvegarder les changements effectué dans les reglages de MariaDB, ou pas ?
oui ça me semble être une bonne idée
```bash=
Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```
THE END !

oui mais non pas encore, occupons nous plus tot de la création de l'utilisateur, avec son ip de connection, et son password...

```bash=
sudo mysql -u root -p

# Création d'un utilisateur dans la base, avec un mot de passe
# L'adresse IP correspond à l'adresse IP depuis laquelle viendra les connexions. Cela permet de restreindre les IPs autorisées à se connecter.
# Dans notre cas, c'est l'IP de web.tp5.linux
# "meow" c'est le mot de passe :D
CREATE USER 'nextcloud'@'10.5.1.11' IDENTIFIED BY 'meow';

# Création de la base de donnée qui sera utilisée par NextCloud
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

# On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.5.1.11';

# Actualisation des privilèges
FLUSH PRIVILEGES;
```

## partie web.tp5.linux


### test de mysql dans la vm web 


recherche des emplacements du packet mysql avec "dnf provides mysql"
```bash=
dnf provides mysql
Rocky Linux 8 - AppStream                                                            2.8 MB/s | 8.2 MB     00:02
Rocky Linux 8 - BaseOS                                                               2.9 MB/s | 3.5 MB     00:01
Rocky Linux 8 - Extras                                                                21 kB/s |  10 kB     00:00
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
```

instalation... avec "sudo dnf install mysql"

```bash=
 sudo dnf install mysql
...
Complete!

///petite erreur de méthode///

 sudo dnf install mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
Last metadata expiration check: 23:23:26 ago on Thu 25 Nov 2021 03:29:01 PM CET.
Package mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!

///rectifié///
```

maintenant que c'est bon, on test si ça fonctionne.
```bash=
[baptiste@web ~]$ mysql --host=10.5.1.12 --port=3306 -p --user=nextcloud nextcloud
Enter password:
...

mysql> SHOW TABLES;
Empty set (0.00 sec)
````

ça fonctionne bien !

### toujours sur la vm web, passons à Apache

*apache ou httpd*

il faut l'instaler.

```bash=
sudo dnf install httpd
Complete!
```

on vérifi l'états du service.
avec la commande "systemctl status httpd"

```bash=
[baptiste@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
     Docs: man:httpd.service(8)


activation du service
```

alors on l'active.
avec la commande "systemctl start httpd"

```bash=
[baptiste@web ~]$ systemctl start httpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'httpd.service'.
Authenticating as: baptiste
Password:
==== AUTHENTICATION COMPLETE ====
```

on activation le service des le debut.
avec la commande "systemctl enable httpd"

```bash=
[baptiste@web ~]$ systemctl enable httpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: baptiste
Password:
==== AUTHENTICATION COMPLETE ====
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: baptiste
Password:
==== AUTHENTICATION COMPLETE ====
```


on trouve le port d'ecoute, avec la commande "sudo ss -ulntp | grep httpd"

```bash=
[baptiste@web ~]$ sudo ss -ulntp | grep httpd
tcp   LISTEN 0      128                *:80              *:*    users:(("httpd",pid=1982,fd=4),("httpd",pid=1981,fd=4),("httpd",pid=1980,fd=4),("httpd",pid=1978,fd=4))

tcp         LISTEN       0             128                              *:80                            *:*           
users:(("httpd",pid=1982,fd=4),("httpd",pid=1981,fd=4),("httpd",pid=1980,fd=4),("httpd",pid=1978,fd=4))
```

le port d'Apache/httpd est le port 80.

pour trouver l'utilisateur du service on fait la commande "ps -ef | grep httpd"
et on voit que l'utilisateur "apache" lance les processus httpd.

```bash=
[baptiste@web ~]$ ps -ef | grep httpd
root        1978       1  0 16:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1979    1978  0 16:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1980    1978  0 16:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1981    1978  0 16:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1982    1978  0 16:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
baptiste    2244    1443  0 16:34 pts/0    00:00:00 grep --color=auto httpd
```

alors on ouvre le port 80, vérrouillé par le firewall.
avec la commande "sudo firewall-cmd --add-port=80/tcp --permanent"
```bash=
[baptiste@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
[sudo] password for baptiste:
success
```
on reload le firewall, pour conserver le port 80 ouvert.
avec la commande "sudo firewall-cmd --reload"
```bash=
[baptiste@web ~]$ sudo firewall-cmd --reload
success
```

alors on vérifient ce qui se passe si on recherche l'ip de la vm dans un navigateur ou avec "curl 10.5.1.11"
```bash=
curl 10.5.1.11
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
  background: rgb(20,72,50);
  background: -moz-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 9
0%)  ;
  background: -webkit-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1
) 90%) ;
  background: linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%);
  background-repeat: no-repeat;
  background-attachment: fixed;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#3c6eb4",end
Colorstr="#3c95b4",GradientType=1);
        color: white;
        font-size: 0.9em;
        font-weight: 400;
        font-family: 'Montserrat', sans-serif;
        margin: 0;
        padding: 10em 6em 10em 6em;
        box-sizing: border-box;

      }


  h1 {
    text-align: center;
    margin: 0;
    padding: 0.6em 2em 0.4em;
    color: #fff;
    font-weight: bold;
    font-family: 'Montserrat', sans-serif;
    font-size: 2em;
  }
  h1 strong {
    font-weight: bolder;
    font-family: 'Montserrat', sans-serif;
  }
  h2 {
    font-size: 1.5em;
    font-weight:bold;
  }

  .title {
    border: 1px solid black;
    font-weight: bold;
    position: relative;
    float: right;
    width: 150px;
    text-align: center;
    padding: 10px 0 10px 0;
    margin-top: 0;
  }

  .description {
    padding: 45px 10px 5px 10px;
    clear: right;
    padding: 15px;
  }

  .section {
    padding-left: 3%;
   margin-bottom: 10px;
  }

  img {

    padding: 2px;
    margin: 2px;
  }
  a:hover img {
    padding: 2px;
    margin: 2px;
  }

  :link {
    color: rgb(199, 252, 77);
    text-shadow:
  }
  :visited {
    color: rgb(122, 206, 255);
  }
  a:hover {
    color: rgb(16, 44, 122);
  }
  .row {
    width: 100%;
    padding: 0 10px 0 10px;
  }

  footer {
    padding-top: 6em;
    margin-bottom: 6em;
    text-align: center;
    font-size: xx-small;
    overflow:hidden;
    clear: both;
  }

  .summary {
    font-size: 140%;
    text-align: center;
  }

  #rocky-poweredby img {
    margin-left: -10px;
  }

  #logos img {
    vertical-align: top;
  }

  /* Desktop  View Options */

  @media (min-width: 768px)  {

    body {
      padding: 10em 20% !important;
    }

    .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6,
    .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
      float: left;
    }

    .col-md-1 {
      width: 8.33%;
    }
    .col-md-2 {
      width: 16.66%;
    }
    .col-md-3 {
      width: 25%;
    }
    .col-md-4 {
      width: 33%;
    }
    .col-md-5 {
      width: 41.66%;
    }
    .col-md-6 {
      border-left:3px ;
      width: 50%;


    }
    .col-md-7 {
      width: 58.33%;
    }
    .col-md-8 {
      width: 66.66%;
    }
    .col-md-9 {
      width: 74.99%;
    }
    .col-md-10 {
      width: 83.33%;
    }
    .col-md-11 {
      width: 91.66%;
    }
    .col-md-12 {
      width: 100%;
    }
  }

  /* Mobile View Options */
  @media (max-width: 767px) {
    .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6,
    .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
      float: left;
    }

    .col-sm-1 {
      width: 8.33%;
    }
    .col-sm-2 {
      width: 16.66%;
    }
    .col-sm-3 {
      width: 25%;
    }
    .col-sm-4 {
      width: 33%;
    }
    .col-sm-5 {
      width: 41.66%;
    }
    .col-sm-6 {
      width: 50%;
    }
    .col-sm-7 {
      width: 58.33%;
    }
    .col-sm-8 {
      width: 66.66%;
    }
    .col-sm-9 {
      width: 74.99%;
    }
    .col-sm-10 {
      width: 83.33%;
    }
    .col-sm-11 {
      width: 91.66%;
    }
    .col-sm-12 {
      width: 100%;
    }
    h1 {
      padding: 0 !important;
    }
  }


  </style>
  </head>
  <body>
    <h1>HTTP Server <strong>Test Page</strong></h1>

    <div class='row'>

      <div class='col-sm-12 col-md-6 col-md-6 '></div>
          <p class="summary">This page is used to test the proper operation of
            an HTTP server after it has been installed on a Rocky Linux system.
            If you can read this page, it means that the software it working
            correctly.</p>
      </div>

      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>


        <div class='section'>
          <h2>Just visiting?</h2>

          <p>This website you are visiting is either experiencing problems or
          could be going through maintenance.</p>

          <p>If you would like the let the administrators of this website know
          that you've seen this page instead of the page you've expected, you
          should send them an email. In general, mail sent to the name
          "webmaster" and directed to the websites domain should reach the
          appropriate person.</p>

          <p>The most common email address to send to is:
          <strong>"webmaster@example.com"</strong></p>

          <h2>Note:</h2>
          <p>The Rocky Linux distribution is a stable and reproduceable platform
          based on the sources of Red Hat Enterprise Linux (RHEL). With this in
          mind, please understand that:

        <ul>
          <li>Neither the <strong>Rocky Linux Project</strong> nor the
          <strong>Rocky Enterprise Software Foundation</strong> have anything to
          do with this website or its content.</li>
          <li>The Rocky Linux Project nor the <strong>RESF</strong> have
          "hacked" this webserver: This test page is included with the
          distribution.</li>
        </ul>
        <p>For more information about Rocky Linux, please visit the
          <a href="https://rockylinux.org/"><strong>Rocky Linux
          website</strong></a>.
        </p>
        </div>
      </div>
      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>
        <div class='section'>

          <h2>I am the admin, what do I do?</h2>

        <p>You may now add content to the webroot directory for your
        software.</p>

        <p><strong>For systems using the
        <a href="https://httpd.apache.org/">Apache Webserver</strong></a>:
        You can add content to the directory <code>/var/www/html/</code>.
        Until you do so, people visiting your website will see this page. If
        you would like this page to not be shown, follow the instructions in:
        <code>/etc/httpd/conf.d/welcome.conf</code>.</p>

        <p><strong>For systems using
        <a href="https://nginx.org">Nginx</strong></a>:
        You can add your content in a location of your
        choice and edit the <code>root</code> configuration directive
        in <code>/etc/nginx/nginx.conf</code>.</p>

        <div id="logos">
          <a href="https://rockylinux.org/" id="rocky-poweredby"><img src= "icon
s/poweredby.png" alt="[ Powered by Rocky Linux ]" /></a> <!-- Rocky -->
          <img src="poweredby.png" /> <!-- webserver -->
        </div>
      </div>
      </div>

      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark o
f <a href="https://apache.org">the Apache Software Foundation</a> in the United
States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of
<a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```

maintenant qu'on en a presque terminé avec httpd, on instale epel-release.

```bash=
sudo dnf install epel-release
...
Complete!

sudo dnf update
...
Complete!

sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
...
Installed:
  remi-release-8.5-1.el8.remi.noarch

Complete!

sudo dnf module enable php:remi-7.4
...
Complete!

sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Complete!
```
ah les fichiers de conf.d
```bash=
cat /etc/httpd/conf/httpd.conf | grep conf.d

# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
```

maintenant on change les permissions des fichiers du netcloud pour que "apache" en soit le propriétaire.
avec la commande "sudo chown apache /var/www/nextcloud/html"

```bash=
sudo chown apache /var/www/nextcloud/html
[sudo] password for baptiste:
```

maintenant on install "nextcloud-21.0.1.zip"
avec la commande "curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip"
```bash=
[baptiste@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
[baptiste@web ~]$ ls
nextcloud-21.0.1.zip
```
le fichier est bien installé, mais il faut le décomprésser, avec la commande "unzip nextcloud-21.0.1.zip -x"
```bash=
[baptiste@web ~]$ unzip nextcloud-21.0.1.zip -x
...
[baptiste@web ~]$ ls
nextcloud  nextcloud-21.0.1.zip
```

a présent, il faut supprimer le fichier zip.
avec la commande "sudo rm -rf nextcloud-21.0.1.zip"
```bash=
[baptiste@web ~]$ sudo rm -rf nextcloud-21.0.1.zip
[baptiste@web ~]$ ls
nextcloud
```
maintenant on déplace le fichier.
```bash=
sudo mv /home/baptiste/nextcloud-21.0.1 /var/www/nextcloud-21.0.1
[sudo] password for baptiste:
```
on donne les droits sur le fichier a "apache"
avec la commande "sudo chown apache /var/www/nextcloud-21.0.1"
```bash=
sudo chown apache /var/www/nextcloud-21.0.1
```

on constate bien que le fichier est bien la !
```bash=
[baptiste@web www]$ ls
cgi-bin  html  nextcloud  nextcloud-21.0.1
```

on change le contenu du fichier "hosts" dans le pc
```bash=
Utilisateur@DESKTOP-KF3I1C4 MINGW64 /c/Windows/System32/drivers/etc
[baptiste@web www]$ cat hosts
10.5.1.11 web.tp5.linux
```
FIN !

