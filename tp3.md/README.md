# tp 3 linux

## 1er VM

```bash=
/srv/idcard$ ls
 idcard.sh  'idcard.sh*'
```

```bash=
yo=$(hostname)
echo "Machine name :  $yo"
// donne le nom de la machine 

distrib1=$(cat /etc/*-release | grep DISTRIB_ID= | cut -d"=" -f2) 
versionlinux=$(/etc/*-release | grep DISTRIB_RELEASE | cut -d"=" -f2)
echo "OS $distrib1 and Kernal version is $versionlinux"
// donne la distribution de la machine et de sa version

nameip=$(ip a | grep inet | grep enp0s8 | awk '{print $2}')
echo "IP : $nameip"
//donne l adresse ipv4 de la machine 

used=$(free -mh | grep Mem: | awk '{print $2}')
free=$(free -mh | grep Mem: | awk '{print $4}')
echo "RAM : $used RAM restante sur $free RAM totale"
// donne la RAM restante, et uttilisé 

disque=$(df -H | grep /dev/sda5 | awk '{print $4}')
echo "Disque : $disque space left"
//donne lespace libre sur le disque

proc=$(ps -eo pmem,pcpu,pid,args | tail -n +2 | sort -rnk 2 | head -n 6)
echo "Top 5 processes by RAM usage :"
echo  "$proc"
//donne les 5 processus qui uttilisent le plus de RAM

ports=$(sudo ss -lanpt | cut -d":" -f2 | cut -d" " -f1)
process=$(sudo ss -lanpt 2> /dev/null | cut -d"(" -f3 | cut -d"," -f1)
ports_array=("$ports")
process_array=("$process")
echo "Listening ports :"
for item in ports; do
    echo "$ports${item} $process${item}"
done
//est censé donner les ports découte, et les processus associés à ces ports

lama=$(curl https://api.thecatapi.com/v1/images/search --silent | cut -d'"' -f10 )
echo "here's $lama"
// donne un URL d image aléatoire de chat


```

voici l'output 

```bash=
bash /srv/idcard.

Machine name :  baptiste-VirtualBox
idcard.sh: ligne 6: /etc/lsb-release: Permission non accordée
OS Ubuntu and Kernal version is
IP : 192.168.183.17/24
RAM : 974Mi RAM restante sur 81Mi RAM totale
Disque : 1,7G space left
Top 5 processes by RAM usage :
25.8  1.4    1597 /usr/bin/gnome-shell
 5.5  0.1    1769 /usr/libexec/evolution-data-server/evolution-alarm-notify
 3.6  0.0    2035 update-notifier
 3.2  0.4    1630 /usr/libexec/ibus-extension-gtk3
 3.2  0.0    1730 /usr/libexec/gsd-media-keys
 3.1  0.1    1761 /usr/libexec/gsd-xsettings
Listening ports :
Port
53
22
631
22ports State    Recv-Q    Send-Q        Local Address:Port        Peer Address:Port    Process                      
"systemd-resolve"
"sshd"
"cupsd"
"sshd"
"sshd"
"cupsd"ports
here's https://cdn2.thecatapi.com/images/O3Iw_6kYG.jpg
```






## 2ème VM

### yt.sh

```bash=
/srv/yt$ ls
downloads  yt.sh
```

```bash=
sudo nano yt.sh


URL="$1" 

title=$(youtube-dl --get-title --skip-download "$URL")
//prend seulement nom de la vidéo

mkdir /srv/yt/downloads >/dev/null 2>&1
mkdir /srv/yt/downloads/"$title" >/dev/null 2>&1
mkdir /srv/yt/downloads/"$title"/"$title".mp4 >/dev/null 2>&1
mkdir /srv/yt/downloads/"$title"/description >/dev/null 2>&1
//si le dossier et les fichiers existent alors le message 
//est renvoyé vers >/dev/null
//sinon il crée les ficiers et dossier


youtube-dl --output/srv/yt/downloads/"$title"/description --skip-download --get-description "$URL" >/dev/null
//prend seulement la description de la vidéo

youtube-dl --output/srv/yt/downloads/"$title"/"$title".mp4 -v --external-downloader "$URL" >/dev/null
//telecharge la vidéo dans '/srv/yt/downloads/"$title"/"$title".mp4'

echo "Video https: $URL was downloaded."
echo "File path : /srv/yt/downloads/$title/$title.mp4"
//renvoi un message personnalisé avec l'URL et l'emplacement du //fichier contenant la vidéo

date1=$(date | cut -d" " -f1)
date2=$(date | cut -d" " -f2)
date3=$(date | cut -d" " -f3)
date4=$(date | cut -d" " -f4)
date5=$(date | cut -d" " -f5)

date 1 à 5 correspond à la date... sans le sur plus de la commande

["$date1 $date2 $date3 $date4 $date5"] Video "$URL" was downloaded. File path : /srv/yt/downloads/"$title"/"$title".mp4

```

+ résultat :

```bash=
/srv/yt$ bash -x yt.sh https://www.youtube.com/watch?v=UaYZieeCX68
//le -x est très important !

+ URL='https://www.youtube.com/watch?v=CMSc8VAsgQk'
++ youtube-dl --get-title --skip-download 'https://www.youtube.com/watch?v=CMSc8VAsgQk'
+ title='Ariane 5 rocket launches SES-17 and French military satellites'
+ mkdir /srv/yt/downloads
+ mkdir '/srv/yt/downloads/Ariane 5 rocket launches SES-17 and French military satellites'
+ mkdir '/srv/yt/downloads/Ariane 5 rocket launches SES-17 and French military satellites/Ariane 5 rocket launches SES-17 and French military satellites.mp4'
+ mkdir '/srv/yt/downloads/Ariane 5 rocket launches SES-17 and French military satellites/description'
+ youtube-dl '--output/srv/yt/downloads/Ariane 5 rocket launches SES-17 and French military satellites/description' --skip-download --get-description 'https://www.youtube.com/watch?v=CMSc8VAsgQk'
Usage: youtube-dl [OPTIONS] URL [URL...]

youtube-dl: error: no such option: --output/srv/yt/downloads/Ariane 5 rocket launches SES-17 and French military satellites/description
+ youtube-dl '--output/srv/yt/downloads/Ariane 5 rocket launches SES-17 and French military satellites/Ariane 5 rocket launches SES-17 and French military satellites.mp4' -v --external-downloader 'https://www.youtube.com/watch?v=CMSc8VAsgQk'
Usage: youtube-dl [OPTIONS] URL [URL...]

youtube-dl: error: no such option: --output/srv/yt/downloads/Ariane 5 rocket launches SES-17 and French military satellites/Ariane 5 rocket launches SES-17 and French military satellites.mp4
+ echo 'Video https: https://www.youtube.com/watch?v=CMSc8VAsgQk was downloaded.'
Video https: https://www.youtube.com/watch?v=CMSc8VAsgQk was downloaded.
+ echo 'File path : /srv/yt/downloads/Ariane 5 rocket launches SES-17 and French military satellites/Ariane 5 rocket launches SES-17 and French military satellites.mp4'
File path : /srv/yt/downloads/Ariane 5 rocket launches SES-17 and French military satellites/Ariane 5 rocket launches SES-17 and French military satellites.mp4
++ cut '-d ' -f1
++ date
+ date1=mar.
++ cut '-d ' -f2
++ date
+ date2=23
++ cut '-d ' -f3
++ date
+ date3=nov.
++ cut '-d ' -f4
++ date
+ date4=2021
++ cut '-d ' -f5
++ date
+ date5=14:05:06
yt.sh: ligne 32: fin de fichier (EOF) prématurée lors de la recherche du « " » correspondant
yt.sh: ligne 37: erreur de syntaxe : fin de fichier prématurée
```
### yt_v2.sh

```bash=
/srv/yt$ ls
 downloads   url  yt.sh  yt_v2.sh
```
+ url est le nom du fichier dans lequel je met les URL des vidéos

```bash=

URL=$(/srv/yt/url | grep youtube.com)
sudo sed '2,4!d' file
// séléctionne la ligne ou se trouve un lien vers youtube,
//puis quelque soit le lien, il supprime la ligne

title=$(youtube-dl --get-title --skip-download "$URL")

mkdir /srv/yt/downloads >/dev/null 2>&1
mkdir /srv/yt/downloads/"$title" >/dev/null 2>&1
mkdir /srv/yt/downloads/"$title"/"$title".mp4 >/dev/null 2>&1
mkdir /srv/yt/downloads/"$title"/description >/dev/null 2>&1

youtube-dl --output/srv/yt/downloads/"$title"/description --skip-download --get-description "$URL" >/dev/null
youtube-dl --output/srv/yt/downloads/"$title"/"$title".mp4 -v --external-downloader "$URL" >/dev/null

echo "Video https: $URL was downloaded."
echo "File path : /srv/yt/downloads/$title/$title.mp4"

date1=$(date | cut -d" " -f1)
date2=$(date | cut -d" " -f2)
date3=$(date | cut -d" " -f3)
date4=$(date | cut -d" " -f4)
date5=$(date | cut -d" " -f5)

["$date1 $date2 $date3 $date4 $date5"] Video "$URL" was downloaded. File path : /srv/yt/downloads/"$title"/"$title".>

sleep 10
bash yt_v2.sh
//tentative de réexécuter yt_v2.sh toutes les 10 secondes

```

le fichier url
```bash=
https://www.youtube.com/watch?v=nmjH3eN3otM




...
```
//contient les lien.

+ resultat :
```bash=
/srv/yt$ bash yt_v2.sh


yt_v2.sh: ligne 2: /srv/yt/url: Permission non accordée
https://www.youtube.com/watch?v=nmjH3eN3otM
ERROR: u'' is not a valid URL. Set --default-search "ytsearch" (or run  youtube-dl "ytsearch:" ) to search YouTube
Usage: youtube-dl [OPTIONS] URL [URL...]

youtube-dl: error: no such option: --output/srv/yt/downloads//description
Usage: youtube-dl [OPTIONS] URL [URL...]

youtube-dl: error: no such option: --output/srv/yt/downloads//.mp4
Video https:  was downloaded.
File path : /srv/yt/downloads//.mp4
yt_v2.sh: ligne 33: fin de fichier (EOF) prématurée lors de la recherche du « " » correspondant
yt_v2.sh: ligne 39: erreur de syntaxe : fin de fichier prématurée
```
