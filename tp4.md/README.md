# tp 4 linux

l'IP de la VM est reconfiguré. 

```bash=
[baptiste@localhost ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:9c:db:ed brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 84706sec preferred_lft 84706sec
    inet6 fe80::a00:27ff:fe9c:dbed/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a1:22:62 brd ff:ff:ff:ff:ff:ff
    inet 10.250.1.56/24 brd 10.250.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fea1:2262/64 scope link
       valid_lft forever preferred_lft forever
```

voici l'interieur du fichier de configuration de enp0s8 après les modifications du fichier.

```bash=
[baptiste@localhost network-scripts]$ cat ifcfg-enp0s8
NAME=enp0s8
DEVICE=enp0s8
BOOTPROTO=static
ONBOOT=yes
IPADDR=10.250.1.56
NETMASK=255.255.255.0
```

pour plus de sécuriser et de facilitée la VM, il a fallu utiliser une paire de clé ssh.
voici la clée publique ssh.
```bash=
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCrwfUDU+iZzaOmjHISkryJB9Q8Zz5SnUnOYv/Fn3mIhz9Z646HJJ2LXVstVWcvVsWS74p64uJ7hZogaHKbaX+xoMSlykUUlJES3YvVxtBzFeR5bsWosmxeAPJHC0lr4u8NM6uDSvdZnoDI6X0Eh45xeTHJOdoWDRDZqAC0DAU4mPUraWfzR7dUNgk95fwdS9MfUF/l19rpIPhFL4jQuhzpZ23BjrxLySWUTj4ZLzBnfem5RDEq3rpN1bosqx+UtWVx08qDlhUnAj+vTFOxxqyus5QfgxqGaqm3NoJkNM2VzBCSYvhniNZpaIly6DZDwnpZZD5AUywaTs8tFPEengbbHDMmowMB6e1NPNsmP4Qz/zfRkIm7RNBylkCxClip40xDRK22HQhrKfFOBHHrwGnohdAHNhm6x+4nidFQl3mtD3d9mtnJTRhMlIz2w+rOelAgrsRxICyUgGwYMUGI/qNZLGUidYZB1JMPjw4MRr5shkZzw4vKB0b3ebqrjZTyJB+YnhJJCN0s+X5uyFyJHlYV3DLaCkFZrnpyMuCkOMYaVv5+FUNlnKeGIZi9jm+Qrq659+K7vPcrblkiElkvG8L4NuL+uyQw0U26kLLRskQ76kpzsJlMYfrCbsILPxs9V+Jpgg5+t8p+UotEiS7Z7rClqylQYc0zV4RCqbOCh+X+mQ== Utilisateur@DESKTOP-KF3I1C4
```
et après il a fallu la copier dans la clée ssh.
d'ailleur, voici l'état du service sshd

```bash=
systemctl status sshd | grep Active:
Active: active (running) since Tue 2021-11-23 16:20:11 CET; 1h 32min ago
```

après, pour tester l'accès à interet de la VM, on fait la commade ping...

```bash=
ping google.com
PING google.com (142.250.201.174) 56(84) bytes of data.
64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=1 ttl=113 time=25.9 ms
64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=2 ttl=113 time=26.7 ms
64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=3 ttl=113 time=25.5 ms
64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=4 ttl=113 time=25.0 ms
64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=5 ttl=113 time=25.4 ms
64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=6 ttl=113 time=27.8 ms
64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=7 ttl=113 time=26.7 ms
64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=8 ttl=113 time=26.9 ms
^C
--- google.com ping statistics ---
8 packets transmitted, 8 received, 0% packet loss, time 7007ms
rtt min/avg/max/mdev = 25.036/26.257/27.845/0.895 ms
```

après, on peut changer le nom de la vm

```bash=
[baptiste@localhost network-scripts]$ cat /etc/hostname
node1.tp4.linux
```

pour voir les modifications, on peut utiliser la commande "shutdown -c" 
puis se reconnecter.

```bash=
[baptiste@node1 ~]$ hostname
node1.tp4.linux
```
voici la modification.


# instalation de nginx

```bash=
 rpm -q nginxrpm -q nginx
package nginxrpm is not installed
package nginx is not installed
```

avec cette commande, on vérifie si nginx est instalé.
si NGINX n'est pas instalé, on peut l'instaler avec :

```bash=
sudo dnf install nginx -y
...
Complete!
```

lorsque c'est fait, il faut lancer le service avec "systemctl start nginx"

```bash=
systemctl start nginx
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'nginx.service'.
Authenticating as: baptiste
Password:
==== AUTHENTICATION COMPLETE ====
```
si on ajoute rien, lors du prochain login il faudrat recommenser alors on peut utiliser la commande "systemctl enable nginx" qui permet de demarrer nginx au demarrage du systeme.

```bash=
systemctl enable nginx
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: baptiste
Password:
==== AUTHENTICATION COMPLETE ====
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: baptiste
Password:
==== AUTHENTICATION COMPLETE ====
```

à présent, si on veut uttiliser nginx, il faut utiliser la commande:
"sudo firewall-cmd --permanent --add-service={http,https}" qui permet d'ouvrir les ports pour http et https.

```bash=
sudo firewall-cmd --permanent --add-service={http,https}
Warning: ALREADY_ENABLED: http
Warning: ALREADY_ENABLED: https
success
```

puis, pour sauvegarder les changements de firewall, il faut utiliser la commande: " sudo firewall-cmd --reload"
```bash=
 sudo firewall-cmd --reload
success
```

pour la suite, il faut aller a /usr/share/nginx/html/
avec la commande "cd /usr/share/nginx/html/"

puis il faut aller dans index.html dans l'anuaire "/usr/share/nginx/html/index.html"
pour y ajouter :
```bash=
<center><h4>My TechBrown Website</h4></center>
```

puis, dans le fichier techbrown.com.conf, on inscrit le scripte ci-dessous, comprenant le nom du serveur à la 9ème ligne.
```bash=
cat techbrown.com.conf
server {
        listen 80;
        listen [::]:80;

        root /usr/share/nginx/html;
        index index.html index.htm;

        serv-tp4-lin server.techbrown.com;

        location / {
                try_files $uri $uri/ =404;
        }
}
```

a present on vérifient le bon fonctionnement de NGINX
avec la commande "sudo nginx -t"
```bash=
sudo nginx -t
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

puis on restart le service NGINX et recharge les firewall.

```bash=
systemctl restart nginx
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to restart 'nginx.service'.
Authenticating as: baptiste
Password:
==== AUTHENTICATION COMPLETE ====


sudo firewall-cmd --reload
success
```

puis testons nginx
depuis un moteur de recherche.
"http://serv-tp4-lin.techbrown.com"
proxy error


depuis un moteur le terminal de la VM.
```bash=
curl firefox "http://serv-tp4-lin.techbrown.com"
proxy error
proxy error
```

voyons à présent l'état du service nginx
avec la commande "ps -ef | grep nginx"
```bash=
ps -ef | grep nginx
root         914       1  0 23:18 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx        915     914  0 23:18 ?        00:00:00 nginx: worker process
baptiste    1517    1480  0 23:20 pts/0    00:00:00 grep --color=auto nginx
```

voyons à présent les ports du service.

```bash=
 sudo ss -ltnp | grep nginx
LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=915,fd=8),("nginx",pid=914,fd=8))
LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=915,fd=9),("nginx",pid=914,fd=9))

```

voici le fichier de configuration de nginx "
"/etc/nginx/nginx.conf"

```bash=
cat /etc/nginx/nginx.conf

# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }

# Settings for a TLS enabled server.
#
#    server {
#        listen       443 ssl http2 default_server;
#        listen       [::]:443 ssl http2 default_server;
#        server_name  _;
#        root         /usr/share/nginx/html;
#
#        ssl_certificate "/etc/pki/nginx/server.crt";
#        ssl_certificate_key "/etc/pki/nginx/private/server.key";
#        ssl_session_cache shared:SSL:1m;
#        ssl_session_timeout  10m;
#        ssl_ciphers PROFILE=SYSTEM;
#        ssl_prefer_server_ciphers on;
#
#        # Load configuration files for the default server block.
#        include /etc/nginx/default.d/*.conf;
#
#        location / {
#        }
#
#        error_page 404 /404.html;
#            location = /40x.html {
#        }
#
#        error_page 500 502 503 504 /50x.html;
#            location = /50x.html {
#        }
#    }

}

```

nous pouvons y voir le port d'écoute.

```bash=
server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;
```

et nous pouvons aussi trouver les logs de NGINX.
avec la commande "sudo journalctl -xe -u nginx"
```bash=
sudo journalctl -xe -u nginx
...
~
~
-- Logs begin at Wed 2021-11-24 23:18:30 CET, end at Thu 2021-11-25 00:02:11 CET. --
Nov 24 23:18:46 node1.tp4.linux systemd[1]: Starting The nginx HTTP and reverse proxy server...
-- Subject: Unit nginx.service has begun start-up
-- Defined-By: systemd
-- Support: https://access.redhat.com/support
--
-- Unit nginx.service has begun starting up.
Nov 24 23:18:47 node1.tp4.linux nginx[892]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Nov 24 23:18:47 node1.tp4.linux nginx[892]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Nov 24 23:18:47 node1.tp4.linux systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid arg>Nov 24 23:18:47 node1.tp4.linux systemd[1]: Started The nginx HTTP and reverse proxy server.
-- Subject: Unit nginx.service has finished start-up
-- Defined-By: systemd
-- Support: https://access.redhat.com/support
--
-- Unit nginx.service has finished starting up.
--
-- The start-up result is done.
~
```

nous pouvons utiliser cette commande:
"sudo firewall-cmd --add-port=80/tcp --permanent"
pour: ouvrir le port 80
et recharger les firewall "sudo firewall-cmd --reload"

```bash=
 sudo firewall-cmd --add-port=80/tcp --permanent
success


sudo firewall-cmd --reload
success
```

puis on test le serveur, avec "http://ip"

```bash=
test http...
http://10.250.1.56:80
"This page is used to test the proper operation of the nginx HTTP server after it has been installed."
```

maintenant que ça fonctionne, on peut tester de changer le ports du service, de 80 à 8080
que l'on peut voir grace à la commande "cat /etc/nginx/nginx.conf | grep default_server"
```bash=
cat /etc/nginx/nginx.conf | grep default_server
        listen       8080 default_server;
        listen       [::]:8080 default_server;
#        listen       443 ssl http2 default_server;
#        listen       [::]:443 ssl http2 default_server;
```

et voici les log du service.
```bash=

Nov 25 00:22:23 node1.tp4.linux systemd[1]: nginx.service: Succeeded.
Nov 25 00:22:23 node1.tp4.linux systemd[1]: Stopped The nginx HTTP and reverse proxy server.
Nov 25 00:22:23 node1.tp4.linux systemd[1]: Starting The nginx HTTP and reverse proxy server...
Nov 25 00:22:23 node1.tp4.linux nginx[1705]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Nov 25 00:22:23 node1.tp4.linux nginx[1705]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Nov 25 00:22:23 node1.tp4.linux systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid argument
Nov 25 00:22:23 node1.tp4.linux systemd[1]: Started The nginx HTTP and reverse proxy server.

```
nous pouvons voir le changement de ports avec la commande "sudo ss -ltnp | grep nginx"
```bash=
 sudo ss -ltnp | grep nginx
LISTEN 0      128          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=1711,fd=8),("nginx",pid=1710,fd=8))
LISTEN 0      128             [::]:8080         [::]:*    users:(("nginx",pid=1711,fd=9),("nginx",pid=1710,fd=9))
```

maintenant, on crée un nouvel utilisateur nomé "web".
```bash=
[baptiste@node1 ~]$ sudo useradd web -m -s /home/web -u 8080
[sudo] password for baptiste:
```
auquel on ajoute des password, avec la commande "sudo passwd nom_de_l_utilisateut"
```bash=
sudo passwd web
Changing password for user web.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.

```

+ cat /etc/nginx/nginx.conf | grep web
= user nginx web;


maintenant on recharge le service NGINX
```bash=
[baptiste@node1 ~]$ systemctl restart nginx.service
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to restart 'nginx.service'.
Authenticating as: baptiste
Password:
==== AUTHENTICATION COMPLETE ====
```

puis on affiche les processus avec la commande "ps -ef | grep nginx"
```bash=
 ps -ef | grep nginx
root        2103       1  0 01:16 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       2104    2103  0 01:16 ?        00:00:00 nginx: worker process
baptiste    2108    1657  0 01:16 pts/0    00:00:00 grep --color=auto nginx
```

# je n'arrive pas a ajouter web comme nouvel utilisateur en tant que celui qui lance le service.

## il manque "🌞 Changer l'emplacement de la racine Web"
