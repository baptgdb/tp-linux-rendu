 **Changer le nom de la machine**

- **première étape : changer le nom tout de suite, jusqu'à ce qu'on redémarre la machine**
  - la commande tapée `sudo hostname node1`
 
- **deuxième étape : changer le nom qui est pris par la machine quand elle s'allume**
  - il faut inscrire le nom de la machine dans le fichier `/etc/hostname`
  - avec la commande `sudo nano /etc/hostname`
  - dans lequel on inscrit `node1.tp2.linux`

## 3. Config réseau

Pour vérifier que vous avez une configuration réseau correcte (étapes à réaliser DANS LA VM) :

```bash=
$ ip a
#ip de la vm = 192.168.183.12
```

```bash=
$ ping 1.1.1.1
# On teste si la machine sait résoudre des noms de domaine
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=54 time=24.0 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=54 time=25.1 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=54 time=24.4 ms
64 bytes from 1.1.1.1: icmp_seq=4 ttl=54 time=25.2 ms
64 bytes from 1.1.1.1: icmp_seq=5 ttl=54 time=23.6 ms
64 bytes from 1.1.1.1: icmp_seq=6 ttl=54 time=28.3 ms
64 bytes from 1.1.1.1: icmp_seq=7 ttl=54 time=24.7 ms
64 bytes from 1.1.1.1: icmp_seq=8 ttl=54 time=25.0 ms
```
```bash=
$ ping google.com

ping google.com
PING google.com (142.250.74.238) 56(84) bytes of data.
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=114 time=23.8 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=114 time=31.2 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=3 ttl=114 time=24.4 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=4 ttl=114 time=24.2 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=5 ttl=114 time=25.4 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=6 ttl=114 time=24.4 ms
```

Ensuite on vérifie que le PC peut `ping` la machine

```bash=
# Afficher la liste des cartes réseaux, la commande dépend de l'OS
$ ipconfig 
#ip de mon pc = 10.33.16.94
 
#la VM ping le pc
 
ping 10.33.16.94
PING 10.33.16.94 (10.33.16.94) 56(84) bytes of data.
64 bytes from 10.33.16.94: icmp_seq=1 ttl=127 time=0.700 ms
64 bytes from 10.33.16.94: icmp_seq=2 ttl=127 time=1.36 ms
64 bytes from 10.33.16.94: icmp_seq=3 ttl=127 time=1.49 ms
64 bytes from 10.33.16.94: icmp_seq=4 ttl=127 time=1.45 ms
64 bytes from 10.33.16.94: icmp_seq=5 ttl=127 time=1.48 ms
64 bytes from 10.33.16.94: icmp_seq=6 ttl=127 time=1.38 ms
64 bytes from 10.33.16.94: icmp_seq=7 ttl=127 time=1.46 ms
64 bytes from 10.33.16.94: icmp_seq=8 ttl=127 time=1.37 ms
64 bytes from 10.33.16.94: icmp_seq=9 ttl=127 time=1.51 ms

# Ping de l'adresse de la VM dans le host-only
$ ping <IP_VM>

ping 192.168.183.12

Envoi d’une requête 'Ping'  192.168.183.12 avec 32 octets de données :
Réponse de 192.168.183.12 : octets=32 temps<1ms TTL=64
Réponse de 192.168.183.12 : octets=32 temps<1ms TTL=64
Réponse de 192.168.183.12 : octets=32 temps<1ms TTL=64
Réponse de 192.168.183.12 : octets=32 temps=1 ms TTL=64
```




# Partie 1 : SSH


## 1. Installation du serveur

Sur les OS GNU/Linux, les installations se font à l'aide d'un gestionnaire de paquets.

**Installer le paquet `openssh-server`**

- avec une commande `sudo apt install openssh-server`
```bash=
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
openssh-server is already the newest version (1:8.4p1-6ubuntu2).
```


## 2. Lancement du service SSH

**Lancer le service `ssh`**

- avec une commande `systemctl start ssh`
```bash=
systemctl start ssh
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ===
Authentication is required to start 'ssh.service'.
Authenticating as: baptiste,,, (baptiste)
Password:
==== AUTHENTICATION COMPLETE ===
```

- vérifier que le service est actuellement actif avec une commande `systemctl status ssh`
```bash=
Synchronizing state of ssh.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable ssh
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ===
Authentication is required to reload the systemd state.
Authenticating as: baptiste,,, (baptiste)
Password:
==== AUTHENTICATION COMPLETE ===
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ===
Authentication is required to reload the systemd state.
Authenticating as: baptiste,,, (baptiste)
Password:
==== AUTHENTICATION COMPLETE ===
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ===
Authentication is required to manage system service or unit files.
Authenticating as: baptiste,,, (baptiste)
Password:
==== AUTHENTICATION COMPLETE ===
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ===
Authentication is required to reload the systemd state.
Authenticating as: baptiste,,, (baptiste)
Password:
==== AUTHENTICATION COMPLETE ===

```


## 3. Etude du service SSH

**Analyser le service en cours de fonctionnement**

- afficher le statut du *service*
  - avec une commande `systemctl status ssh`
-Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
  - avec une commande `ps`
 ```bash=
  ps -ef | grep ssh
root         542       1  0 15:58 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
root        1502     542  0 16:15 ?        00:00:00 sshd: baptiste [priv]
baptiste    1579    1502  0 16:15 ?        00:00:00 sshd: baptiste@pts/1
baptiste    2817    1580  0 17:37 pts/1    00:00:00 grep --color=auto ssh

 ```

- afficher le port utilisé par le *service* `ssh`
  - avec une commande `sudo ss -ltnp`
```bash=
 sudo ss -ltnp
State              Recv-Q             Send-Q                         Local Address:Port                          Peer Address:Port             Process
LISTEN             0                  4096                           127.0.0.53%lo:53                                 0.0.0.0:*                 users:(("systemd-resolve",pid=410,fd=14))
LISTEN             0                  128                                  0.0.0.0:22                                 0.0.0.0:*                 users:(("sshd",pid=542,fd=3))
LISTEN             0                  128                                127.0.0.1:631                                0.0.0.0:*                 users:(("cupsd",pid=519,fd=7))
LISTEN             0                  128                                     [::]:22                                    [::]:*                 users:(("sshd",pid=542,fd=4))
LISTEN             0                  128                                    [::1]:631                                   [::]:*                 users:(("cupsd",pid=519,fd=6))

```

- afficher les logs du *service* `ssh`
  - avec une commande `journalctl`
```bash=
 journalctl | grep sshd
oct. 19 17:58:33 baptiste-VirtualBox sudo[1606]: baptiste : TTY=pts/0 ; PWD=/home/baptiste ; USER=root ; COMMAND=/usr/bin/systemctl enable --now sshd
oct. 19 17:58:41 baptiste-VirtualBox sudo[1608]: baptiste : TTY=pts/0 ; PWD=/home/baptiste ; USER=root ; COMMAND=/usr/bin/systemctl status sshd
oct. 19 17:58:57 baptiste-VirtualBox sudo[1612]: baptiste : TTY=pts/0 ; PWD=/home/baptiste ; USER=root ; COMMAND=/usr/bin/apt install sshd -y
oct. 19 17:59:13 baptiste-VirtualBox useradd[1840]: new user: name=sshd, UID=123, GID=65534, home=/run/sshd, shell=/usr/sbin/nologin, from=none
oct. 19 17:59:13 baptiste-VirtualBox usermod[1848]: change user 'sshd' password
oct. 19 17:59:13 baptiste-VirtualBox chage[1855]: changed password expiry for sshd
oct. 19 17:59:15 baptiste-VirtualBox sshd[1949]: Server listening on 0.0.0.0 port 22.
oct. 19 17:59:15 baptiste-VirtualBox sshd[1949]: Server listening on :: port 22.
oct. 19 17:59:33 baptiste-VirtualBox sudo[2957]: baptiste : TTY=pts/0 ; PWD=/home/baptiste ; USER=root ; COMMAND=/usr/bin/systemctl enable --now sshd
```

  - en consultant un dossier dans `/var/log/`
  - ne me donnez pas toutes les lignes de logs, je veux simplement que vous appreniez à consulter les logs

---
**Se connecte au serveur depuis un client SSH**
```bash=
ssh baptiste@192.168.183.12
```

## 4. Modification de la configuration du serveur

**Modifier le comportement du service**

- c'est dans le fichier `/etc/ssh/sshd_config`
  - c'est un simple fichier texte
  - modifiez-le comme vous voulez, je vous conseille d'utiliser `sudo nano /etc/ssh/sshd_config` en ligne de commande
- effectuez le modifications suivante :
  - changer le ***port d'écoute*** du service *SSH*
    - peu importe lequel, il doit être compris entre 1025 et 65536
```bash=
  cat -T /etc/ssh/sshd_config
...
#Port 65536
...
```

- pour cette modification, prouver à l'aide d'une commande qu'elle a bien pris effet
  - une commande `ss -l` pour vérifier le port d'écoute
j'ai pas trouve l'option en suplement de ss **-l**, 
```bash=
 sudo ss -ltnp /etp/ssh/sshd_config
Error: an inet prefix is expected rather than "/etp/ssh/sshd_config".
Cannot parse dst/src address.
```
 donc j'ai trouvé une alternative :
```bash=
 grep -i port /etc/ssh/sshd_config
#Port 65536

```

> Pour que les changements inscrits dans le fichier de configuration prennent effet il faut redémarrer le service avec une commande `systemctl restart ssh` 



# Partie 2 : FTP

## 1. Installation du serveur

**Installer le paquet `sudo apt install vsftpd`**
```bash=
sudo apt install vsftpd
[sudo] password for baptiste:
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following NEW packages will be installed:
  vsftpd
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 122 kB of archives.
After this operation, 322 kB of additional disk space will be used.
Get:1 http://fr.archive.ubuntu.com/ubuntu impish/main amd64 vsftpd amd64 3.0.3-13build1 [122 kB]
Fetched 122 kB in 0s (603 kB/s)
Preconfiguring packages ...
Selecting previously unselected package vsftpd.
(Reading database ... 195384 files and directories currently installed.)
Preparing to unpack .../vsftpd_3.0.3-13build1_amd64.deb ...
Unpacking vsftpd (3.0.3-13build1) ...
Setting up vsftpd (3.0.3-13build1) ...
Created symlink /etc/systemd/system/multi-user.target.wants/vsftpd.service → /lib/systemd/system/vsftpd.service.
Processing triggers for man-db (2.9.4-2) ...
```


## 2. Lancement du service FTP

**Lancer le service `vsftpd`**

- avec une commande `systemctl start vsftpd`
```bash=
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ===
Authentication is required to start 'vsftpd.service'.
Authenticating as: baptiste,,, (baptiste)
Password:
==== AUTHENTICATION COMPLETE ===
```

- vérifier que le service est actuellement actif avec une commande `systemctl status vsftpd`
```bash=

systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2021-11-04 21:13:21 CET; 1min 49s ago
    Process: 1599 ExecStartPre=/bin/mkdir -p /var/run/vsftpd/empty (code=exited, status=0/SUCCES>
   Main PID: 1600 (vsftpd)
      Tasks: 1 (limit: 1106)
     Memory: 680.0K
        CPU: 3ms
     CGroup: /system.slice/vsftpd.service
             └─1600 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 04 21:13:21 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 04 21:13:21 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```

> Vous pouvez aussi faire en sorte que le service FTP se lance automatiquement au démarrage avec la commande `systemctl enable vsftpd`.

## 3. Etude du service FTP

🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du service
  - avec une commande `systemctl status vsftpd`
```bash=
systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2021-11-04 21:13:21 CET; 1min 49s ago
    Process: 1599 ExecStartPre=/bin/mkdir -p /var/run/vsftpd/empty (code=exited, status=0/SUCCES>
   Main PID: 1600 (vsftpd)
      Tasks: 1 (limit: 1106)
     Memory: 680.0K
        CPU: 3ms
     CGroup: /system.slice/vsftpd.service
             └─1600 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 04 21:13:21 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 04 21:13:21 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```
- afficher le/les processus liés au service `vsftpd`
  - avec une commande `ps -ef | grep vsftpd `
```bash=
ps -ef | grep vsftpd
root        1600       1  0 21:13 ?        00:00:00 /usr/sbin/vsftpd /etc/vsftpd.conf
baptiste    2172     947  0 21:15 pts/0    00:00:00 systemctl status vsftpd
baptiste    2399     947  0 21:25 pts/0    00:00:00 grep --color=auto vsftpd
```
- afficher le port utilisé par le service `vsftpd`
  - avec une commande `ss -l`
```bash=
 sudo ss -ltnp
State       Recv-Q      Send-Q             Local Address:Port             Peer Address:Port      Process
LISTEN      0           4096               127.0.0.53%lo:53                    0.0.0.0:*          users:(("systemd-resolve",pid=411,fd=14))
LISTEN      0           128                      0.0.0.0:22                    0.0.0.0:*          users:(("sshd",pid=556,fd=3))
LISTEN      0           128                    127.0.0.1:631                   0.0.0.0:*          users:(("cupsd",pid=533,fd=7))
LISTEN      0           32                             *:21                          *:*          users:(("vsftpd",pid=1600,fd=3))
LISTEN      0           128                         [::]:22                       [::]:*          users:(("sshd",pid=556,fd=4))
LISTEN      0           128                        [::1]:631                      [::]:*
```
- afficher les logs du service `vsftpd`
```bash=
 journalctl | grep vsftpd
nov. 04 21:13:16 node1.tp2.linux sudo[1448]: baptiste : TTY=pts/0 ; PWD=/home/baptiste ; USER=root ; COMMAND=/usr/bin/apt install vsftpd
nov. 04 21:13:21 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 04 21:13:21 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
nov. 04 21:14:30 node1.tp2.linux polkitd(authority=local)[465]: Operator of unix-process:2162:36506 successfully authenticated as unix-user:baptiste to gain ONE-SHOT authorization for action org.freedesktop.systemd1.manage-units for system-bus-name::1.92 [systemctl start vsftpd] (owned by unix-user:baptiste)
nov. 04 21:16:51 node1.tp2.linux polkitd(authority=local)[465]: Operator of unix-process:2178:49196 successfully authenticated as unix-user:baptiste to gain ONE-SHOT authorization for action org.freedesktop.systemd1.manage-unit-files for system-bus-name::1.102 [systemctl enable vsftpd] (owned by unix-user:baptiste)
nov. 04 21:16:53 node1.tp2.linux polkitd(authority=local)[465]: Operator of unix-process:2178:49196 successfully authenticated as unix-user:baptiste to gain ONE-SHOT authorization for action org.freedesktop.systemd1.reload-daemon for system-bus-name::1.102 [systemctl enable vsftpd] (owned by unix-user:baptiste)

```
je n'ais pas réussit l'upload, mais j'ai réussit le download
```bash
zgrep "download" /var/log/*
...
/var/log/syslog:Nov  3 15:08:01 node1 systemd[1]: update-notifier-download.service: Deactivated successfully.
/var/log/syslog:Nov  4 21:08:35 node1 systemd[1]: Started Daily apt download activities.
grep: (standard input): binary file matches
/var/log/syslog.1:Oct 19 17:46:52 baptiste-VirtualBox systemd[1]: Started Daily apt download activities.
/var/log/syslog.1:Oct 19 17:51:54 baptiste-VirtualBox systemd[1]: update-notifier-download.service: Deactivated successfully.
/var/log/syslog.1:Oct 20 11:19:42 baptiste-VirtualBox systemd[1]: Started Daily apt download activities.
/var/log/syslog.1:Oct 20 11:23:05 baptiste-VirtualBox systemd[1]: Stopped Daily apt download activities.
/var/log/syslog.1:Oct 20 11:23:05 baptiste-VirtualBox systemd[1]: update-notifier-download.timer: Deactivated successfully.
/var/log/syslog.1:Oct 20 11:28:07 baptiste-VirtualBox systemd[1]: Started Daily apt download activities.
...
```
il a bien été éfféctué.
```bash=
cd Downloads/
baptiste@node1:~/Downloads$ ls
test.txt  text.txt
```

## 4. Modification de la configuration du serveur

Pour modifier comment un service se comporte il faut modifier de configuration. On peut tout changer à notre guise.

**Modifier le comportement du service**

- c'est dans le fichier `/etc/vsftpd.conf`
- effectuez les modifications suivantes :
  - changer le port où écoute `vstfpd`
    avec `sudo nano /etc/vsftpd.conf`

  - vous me prouverez avec un `cat` que vous avez bien modifié cette ligne
```bash
 cat /etc/vsftpd.conf
...
# Make sure PORT transfer connections originate from port 1025 (ftp-data).
connect_from_port_1025=YES
...
```
  - une commande `ss -l` pour vérifier le port d'écoute

```bash=
grep -i port /etc/vsftpd.conf
# Make sure PORT transfer connections originate from port 1025 (ftp-data).
connect_from_port_1025=YES
```

> Vous devez redémarrer le service avec une commande `systemctl restart` pour que les changements inscrits dans le fichier de configuration prennent effet.

```bash=
systemctl restart vsftpd.service
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ===
Authentication is required to restart 'vsftpd.service'.
Authenticating as: baptiste,,, (baptiste)
Password:
==== AUTHENTICATION COMPLETE ===
```

**Connectez vous sur le nouveau port choisi**

```bash=
ls
Desktop  Documents  Downloads  Music  Pictures  Public  ssh  ssh.save  Templates  test.txt  testv2.txt  Videos

pour le download, le fichier modifié est bien sur le pc.
```

# **partie 3**
# II. Jouer avec netcat

- la VM va agir comme un serveur, à l'aide de la commande `netcat`
  - ce sera une commande `nc -l 1234`
- votre PC agira comme le client
  - il faudra avoir la commande `
nc 192.168.183.12 1234` dans le terminal de votre PC


**Utiliser `netcat` pour stocker les données échangées dans un fichier**

```bash=
>ls
baptiste@client:~$ ls
Bureau     Images   Musique  snap             Vidéos
Documents  Modèles  Public   Téléchargements

baptiste@client:~$ ls Documents/
gameshell  gameshell.1  gameshell.2  gameshell-save.sh  gameshell.sh  lama.txt

baptiste@client:~$ cat  Documents/lama.txt
test lama

```


Il sera donc possible de l'utiliser avec `netcat` comme suit :

```bash
$ nc -l 1234 > lama.txt
```
```bash=

ls -l /systemctl
--w------- 1 root root 1 nov.   8 21:36 /systemctl
```

## 1. Créer le service

🌞 **Créer un nouveau service**

- créer le fichier `/etc/systemd/system/chat_tp2.service`
- définissez des permissions identiques à celles des aux autres fichiers du même type qui l'entourent
- déposez-y le contenu suivant :
```bash=
sudo nano /etc/systemd/system/chat_tp2.service
```
```bash=
[Unit]
Description=Little chat service (TP2)

[Service]
ExecStart=nc -lx

[Install]
WantedBy=multi-user.target
```
```bash=
sudo chmod 400 /etc/systemd/system/chat_tp2.service
```
```bash=
sudo systemctl daemon-reload chat_tp2.service
```




###### je n'ai pas réussi a finir la suite


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
## 2. Test test et retest

🌞 **Tester le nouveau service**

- depuis la VM
  - démarrer le nouveau service avec une commande `systemctl start`
  - vérifier qu'il est correctement lancé avec une commande  `systemctl status`
  - vérifier avec une comande `ss -l` qu'il écoute bien derrière le port que vous avez choisi
- tester depuis votre PC que vous pouvez vous y connecter
- pour visualiser les messages envoyés par le client, il va falloir regarder les logs de votre service, sur la VM :

```bash
# Voir l'état du service, et les derniers logs
$ systemctl status chat_tp2

# Voir tous les logs du service
$ journalctl -xe -u chat_tp2

# Suivre en temps réel l'arrivée de nouveaux logs
# -f comme follow :)
$ journalctl -xe -u chat_tp2 -f
```
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
